import java.io.IOException;


public class Main {




	public static void main(String[] args) throws InterruptedException, IOException {



		final Logger[] loggers = new Logger[]
				{
						new ConsoleLogger(),
						new MailLogger(),
						new GUILogger()
				};


		Crawler crawler = new Crawler();
		crawler.sciezka="plik.txt";
		crawler.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
		crawler.addIterationComplitedListener(iteration->System.out.println("Zakonczono iteracje"));

		crawler.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
		//crawler.addStudentaddedListener((Student)->loggers[1].log("ADDED",Student));


		crawler.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));
		//crawler.addStudentremovedListener((Student)->loggers[1].log("REMOVED",Student));

		crawler.addStudentaddedListener((Student)->loggers[2].log("ADDED",Student));
		crawler.addStudentremovedListener((Student)->loggers[2].log("REMOVED",Student));




		try {
			crawler.run();
		}
		catch (CrawlerException e)
		{
			e.pisz();
		}

	}


		}


